#ifndef NRF24L01_CONFIG_H
#define NRF24L01_CONFIG_H

// These macros should be specific to microcontroller
extern void
_delay_us(double __us);

// NOTE: Product specification
// Section: 8.3.1 SPI commands
// Every new command must be started by a high to low transition on CSN

// drop CSN to enable SPI communication
#define NRF24L01P_DropCSN()  PORTB &= 0b11111011
// raise CSN to disable SPI communication
#define NRF24L01P_RaiseCSN() PORTB |= 0b00000100

// NOTE: Check section 6.1.6
// raise CE to initiate RF Transmission
#define NRF24L01P_RaiseCE() PORTD |= 0b00100000
// drop CE to disable RF Transmission
#define NRF24L01P_DropCE()  PORTD &= 0b11011111

// Function to delay in microseconds
#define NRF2401LP_DelayUs( x ) _delay_us( x)
#endif // NRF24L01_CONFIG_H
