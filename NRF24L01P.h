#ifndef NRF24L01P_H
#define NRF24L01P_H

//#include "NRF24L01Config.h"

#define SPI_CMD_RD_REG            0x00
#define SPI_CMD_WR_REG            0x20
#define SPI_CMD_RD_RX_PAYLOAD     0x61
#define SPI_CMD_WR_TX_PAYLOAD     0xa0
#define SPI_CMD_R_RX_PL_WID       0x60
#define SPI_CMD_FLUSH_TX          0xe1
#define SPI_CMD_FLUSH_RX          0xe2

#define SPI_CMD_NOP               0xff
#define TIMING_Tpece2csn_us       4
#define TIMING_Thce_us              10   //  10uS
#define TIMING_Tpd2stby_us        4500   // 4.5mS worst case

// CONFIG register:
#define REG_CONFIG                0x00

#define CONFIG_PRIM_RX        (1<<0)
#define CONFIG_PWR_UP         (1<<1)
#define CONFIG_CRC0           (1<<2)
#define CONFIG_EN_CRC         (1<<3)
#define CONFIG_MASK_MAX_RT    (1<<4)
#define CONFIG_MASK_TX_DS     (1<<5)
#define CONFIG_MASK_RX_DR     (1<<6)

#define REG_EN_AA                 0x01

#define REG_EN_RXADDR             0x02
#define REG_SETUP_RETR            0x04
#define REG_STATUS                0x07
#define REG_RX_PW_P0              0x11

// STATUS register:
#define STATUS_TX_FULL        (1<<0)
#define STATUS_RX_P_NO        (0x7<<1)
#define STATUS_MAX_RT         (1<<4)
#define STATUS_TX_DS          (1<<5)
#define STATUS_RX_DR          (1<<6)

#define TX_FIFO_SIZE   32
#define RX_FIFO_SIZE   32

// EN_AA register:
#define EN_AA_NONE            0

// SETUP_RETR register:
#define SETUP_RETR_NONE       0

template< typename WriteReadInterface>
class NRF24L01P
{
public:

    enum class ChipMode : uint8_t
    {
        UNKNOWN,
        POWER_DOWN,
        STANDBY,
        RX,
        TX
    };

    NRF24L01P( WriteReadInterface* inDevice ) : mSPI( inDevice ) {
        // Raise CSN because SPI transfer is initiated by
        // lowering CSN
        NRF24L01P_RaiseCSN();
    }

    void powerUp( bool inUp )
    {
        unsigned char theConfig = getRegister(REG_CONFIG);

        if( inUp )
        {
            theConfig |= CONFIG_PWR_UP;
            mMode = ChipMode::STANDBY;
        }
        else
        {
            theConfig &= ~CONFIG_PWR_UP;
            mMode = ChipMode::POWER_DOWN;
        }

        setRegister(REG_CONFIG, theConfig);
        // Wait until the nRF24L01+ powers up
        // Check state diagram

        NRF2401LP_DelayUs( TIMING_Tpd2stby_us );
    }

    void setReceiveMode()
    {
        if ( ChipMode::POWER_DOWN == mMode ) powerUp(true);
        uint8_t theConfig = getRegister(REG_CONFIG);
        theConfig |= CONFIG_PRIM_RX;
        setRegister(REG_CONFIG, theConfig);
        mMode = ChipMode::RX;
    }

    void setTransmitMode()
    {
        if ( ChipMode::POWER_DOWN == mMode ) powerUp(true);
        uint8_t theConfig = getRegister(REG_CONFIG);
        theConfig &= ~CONFIG_PRIM_RX;
        setRegister(REG_CONFIG, theConfig);
        mMode = ChipMode::TX;
    }

    void enable(bool inEnable)
    {
        mEnabled = inEnable;
        if ( mEnabled )
        {
            NRF24L01P_RaiseCE();
            NRF2401LP_DelayUs(TIMING_Tpece2csn_us );
        }
        else
        {
            NRF24L01P_DropCE();
        }
    }

    unsigned char getRegister( unsigned char inReg ) {
        NRF24L01P_DropCSN();
        mSPI->writeRead(inReg);
        unsigned char data = mSPI->writeRead(SPI_CMD_NOP);
        NRF24L01P_RaiseCSN();
        return data;
    }

    void setRegister( unsigned char inReg, unsigned char inRegData ) {
        volatile unsigned char cn = (SPI_CMD_WR_REG | inReg);
        NRF24L01P_DropCSN();
        mSPI->writeRead(cn);
        mSPI->writeRead(inRegData);
        NRF24L01P_RaiseCSN();
    }

    int getStatusRegister()
    {
        NRF24L01P_DropCSN();
        unsigned char status = mSPI->writeRead( SPI_CMD_NOP );
        NRF24L01P_RaiseCSN();
        return status;
    }

    // Make sure count is less than or equal to 32
    int8_t write(uint8_t *data, uint8_t count)
    {
        NRF24L01P_DropCSN();

        mSPI->writeRead(SPI_CMD_WR_TX_PAYLOAD);
        for ( int i = 0; i < count; i++ )
            mSPI->writeRead(*data++);
        NRF24L01P_RaiseCSN();
        return count;
    }

    // Make sure count is less than or equal to 32
    // and data buffer is already allocated

    int8_t read( uint8_t *data, uint8_t count)
    {
        NRF24L01P_DropCSN();
        mSPI->writeRead(SPI_CMD_R_RX_PL_WID);
        uint8_t rxPayloadWidth = mSPI->writeRead(SPI_CMD_NOP);
        NRF24L01P_RaiseCSN();

        if ( rxPayloadWidth > RX_FIFO_SIZE )
        {
            NRF24L01P_DropCSN();
            mSPI->writeRead(SPI_CMD_FLUSH_RX);
            mSPI->writeRead(SPI_CMD_NOP);
            NRF24L01P_RaiseCSN();

        }
        else
        {
            if ( rxPayloadWidth < count ) count = rxPayloadWidth;
            NRF24L01P_DropCSN();
            mSPI->writeRead(SPI_CMD_RD_RX_PAYLOAD);
            for ( int i = 0; i < count; i++ )
            {
                *data++ = mSPI->writeRead(SPI_CMD_NOP);
            }
            NRF24L01P_RaiseCSN();
            // Clear the Status bit
            setRegister(REG_STATUS, STATUS_RX_DR);
            return count;
        }
        return -1;
    }

    void flushTxFifo()
    {
        NRF24L01P_DropCSN();
        mSPI->writeRead(SPI_CMD_FLUSH_TX);
        mSPI->writeRead(SPI_CMD_NOP);
        NRF24L01P_RaiseCSN();
    }

    bool readable()
    {
        uint8_t status = getStatusRegister();
        return ( ( status & STATUS_RX_DR ) && ( ( ( status & STATUS_RX_P_NO ) >> 1 ) == 0 ) );
    }


private:
    ChipMode mMode = ChipMode::UNKNOWN;
    bool mEnabled = false;
    WriteReadInterface* mSPI;
};

#endif // NRF24L01P_H
